<?php

namespace atsilex\base;

use Iterator;
use DomainException;
use ReflectionClass;
use Exception;

/**
 * Enforces stricter object semantics than PHP.
 */
abstract class Object implements Iterator
{
    public function __get($name)
    {
        $msg = sprintf('Attempt to read from undeclared property %s.', get_class($this) . '::' . $name);
        throw new DomainException($msg);
    }

    public function __set($name, $value)
    {
        $msg = sprintf('Attempt to write to undeclared property %s.', get_class($this) . '::' . $name);
        throw new DomainException($msg);
    }

    public function current()
    {
        $this->throwOnAttemptedIteration();
    }

    public function key()
    {
        $this->throwOnAttemptedIteration();
    }

    public function next()
    {
        $this->throwOnAttemptedIteration();
    }

    public function rewind()
    {
        $this->throwOnAttemptedIteration();
    }

    public function valid()
    {
        $this->throwOnAttemptedIteration();
    }

    private function throwOnAttemptedIteration()
    {
        $msg = sprintf('Attempting to iterate an object (of class %s) which is not iterable.', get_class($this));
        throw new DomainException($msg);
    }

    /**
     * Read the value of a class constant.
     *
     * @param string $key
     * @return mixed
     * @throws Exception
     */
    public function getClassConstant($key)
    {
        $const = (new ReflectionClass($this))->getConstant($key);
        if ($const === false) {
            $msg = sprintf('"%s" class "%s" must define a "%s" constant.', __CLASS__, get_class($this), $key);
            throw new Exception($msg);
        }

        return $const;
    }
}

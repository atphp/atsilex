<?php

namespace atsilex\module\search;

use atsilex\module\Module;

/**
 * Something similar to Drupal's search_api module.
 */
class SearchModule extends Module
{
    protected $machineName = 'search';
    protected $name        = 'Search';
}

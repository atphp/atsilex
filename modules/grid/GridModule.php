<?php

namespace atsilex\module\search;

use atsilex\module\Module;

/**
 * Make it easy to render the data rendering on web pages.
 */
class GridModule extends Module
{
    protected $machineName = 'grid';
    protected $name        = 'Grid';
    protected $routeFile   = true;
}

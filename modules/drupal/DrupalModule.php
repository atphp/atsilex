<?php

namespace atphp\module\drupal;

use atphp\module\App;
use atphp\module\drupal\controllers\DrupalController;
use atphp\module\drupal\drupal\Drupal;
use atsilex\module\Module;
use atsilex\module\system\ModularApp;
use Pimple\Container;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

/**
 * @TODO On bootstrap Drupal, register custom error handler, let Silex's logger handle/log errors.
 * @TODO Run Drush from silex (php cli.php drush), custom hook implementations must be invoked.
 */
class DrupalModule extends Module
{

    protected $machineName = 'drupal';
    protected $name        = 'Drupal';
    protected $description = 'Provide integration to Drupal.';
    protected $routeFile   = false;

    /**
     * {@inheritdoc}
     */
    public function register(Container $c)
    {
        $c['drupal'] = function (Container $c) {
            require_once __DIR__ . '/drupal/hooks.php';

            $drupal = new Drupal(
                $c['drupal.options']['root'],
                $c['drupal.options']['site_dir'],
                $c['drupal.options']['base_url'],
                $c['drupal.options']['global'],
                $c['drupal.options']['conf']
            );

            drilex_dispatcher($c['dispatcher']);
            drilex_drupal($drupal);

            $drupal
                ->setCache($c['cache'])
                ->setTwig($c['twig']);

            return $drupal;
        };
    }

    /**
     * {@inheritdoc}
     *
     * @param ModularApp $app
     */
    public function connect(Application $app)
    {
        /** @var ControllerCollection $route */
        $route = $app['controllers_factory'];

        return $route;
    }

}
